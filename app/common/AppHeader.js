import React from "react";
import {Image, Platform, SafeAreaView, Text, TouchableOpacity, View} from "react-native";
import arrowBack from '../../assets/img/headerArrowBack.png';

export const AppHeader = props => (
    <SafeAreaView style={{backgroundColor: '#FFFFFF', height: (Platform.OS === 'ios' ? 44 : 56)}}>
        {console.log('PROPS..... ', props)}
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-end'}}>
            <Text style={{textAlign: 'center', color: props.titleTextColor ? props.titleTextColor : '#28706D', fontFamily: 'Montserrat-Medium', fontSize: 16}}>
                {props.title}
            </Text>
            {
                props.scene.index !== 0
                &&
                <TouchableOpacity style={{position: 'absolute', left: '5.6%', height: '100%', alignItems: 'center', justifyContent: 'center'}} onPress={() => {
                    requestAnimationFrame(() => {
                        props.scene.descriptor.navigation.goBack(props.scene.descriptor.key);
                    });
                }}>
                    <Image source={arrowBack} style={{tintColor: '#A6A6A6'}}/>
                </TouchableOpacity>
            }
        </View>
    </SafeAreaView>

);
