import React from "react";
import {View} from "react-native";
import Arivanza from "./Arivanza";

export const NavMenu = props => (
    <View style={{height: '7.5%', marginBottom: '1%', alignItems: 'center'}}>
        <View style={{backgroundColor: '#EBFFFE', height: '100%', width: '97%', borderRadius: Arivanza.absWidth()}}>
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}}>
                {props.children}
            </View>
        </View>
    </View>
);
