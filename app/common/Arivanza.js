import React, {Component} from 'react';
import Constants from '../config/Constants';
import _ from 'lodash';

import {
    Platform, Dimensions
} from 'react-native';
import Util from "./Util";
import Config from "../config/Constants";

const {height, width} = Dimensions.get('window');
const fontScaleRatio = width / Config.DESIGN.WIDTH;
export default class Arivanza extends Component {
    constructor(props, context) {
        super(props, context);
    }

    static absHeight = (percent = 1) => {
        return (height * percent);
    };
    static absWidth = (percent = 1) => {
        return (width * percent);
    };
    static scaleFontSize = (fontSizeAtDesign) => {
        return (fontSizeAtDesign * fontScaleRatio);
    };

    render() {
        return null;
    }
}
