/**
 * Arivanza React Native App
 * @flow
 */

import React from 'react';
import Constants from './config/Constants';
import imgDownload from '../assets/img/download.png';
import {FlatList, Image, PermissionsAndroid, Platform, SafeAreaView, Text, TouchableOpacity, View} from 'react-native';
import SharedGroupPreferences from 'react-native-shared-group-preferences';
import Arivanza from "./common/Arivanza";
import {AppHeader} from "./common/AppHeader";
import iconNetwork from '../assets/img/network.png';
import iconChat from '../assets/img/chat-bubble.png';
import iconCoins from '../assets/img/coins.png';
import iconInbox from '../assets/img/letterbox.png';
import iconEdition from '../assets/img/edit.png';
import Video from 'react-native-video';
import NavigationBar from 'react-native-navbar-color'
import {NavMenu} from "./common/NavMenu";
import WebView from "react-native-android-fullscreen-webview-video";

export default class Inbox extends Arivanza {
    constructor(props, context) {
        super(props, context);
        this.state = {
            inboxItems: [],
            showClippings: false
        };
        this.player = null;
    }

    static navigationOptions = ({navigation}) => {
        return {
            header: (props) => <AppHeader {...props} title={'Inbox'}/>,
        };
    };

    async dealWithPermissions() {
        try {
            const grantedStatus = await PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            ]);
            const readGranted = grantedStatus["android.permission.READ_EXTERNAL_STORAGE"] === PermissionsAndroid.RESULTS.GRANTED
            if (!readGranted) {
                //TODO
                // You can either limit the user in access to the app's content,
                // or do a workaround where the user's data is saved using only
                // within the user's local app storage using something like AsyncStorage
                // instead. This is only an android issue since it uses read/write external storage.
            }
        } catch (err) {
            console.warn(err)
        }
    }

    async componentDidMount() {
        //set nav bar color
        NavigationBar.setColor('#4ED7D0');
        try {
            if(Platform.OS === 'android') {
                this.dealWithPermissions();
            }
            const loadedData = await SharedGroupPreferences.getItem(Constants.KEY_DB_INBOX, Constants.APP_GROUP_IDENTIFIER);
            console.log('got Arivanza......', loadedData);
            this.setState({inboxItems: loadedData});
        } catch (errorCode) {
            // errorCode 0 = no group name exists. You probably need to setup your Xcode Project properly.
            // errorCode 1 = there is no value for that key
            console.log(errorCode);
            this.setState({inboxItems: []});
        }
    }

    showClippings = () => {
        this.setState({showClippings: true});
    };

    render() {
        const {navigate} = this.props.navigation;
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: '#4ED7D0'}}>
                <View style={{flex: 1}}>
                    {
                        //default view of inbox
                        !this.state.showClippings
                        &&
                        <View style={{height: '20%', marginLeft: '4%', marginTop: '4%', width: '48%'}}>
                            <View style={{flex: 1, backgroundColor: '#FFC95C', alignItems: 'center', justifyContent: 'center', borderTopLeftRadius: 4, borderTopRightRadius: 4}}>
                                <Text style={{textAlign: 'center', fontFamily: 'Montserrat-Medium', fontSize: Arivanza.scaleFontSize(17)}}>CLIPPINGS</Text>
                            </View>
                            <TouchableOpacity
                                style={{
                                    flex: 2.375,
                                    backgroundColor: '#FFFFFF',
                                    alignItems: 'center',
                                    justifyContent: 'flex-end',
                                    borderBottomLeftRadius: 4,
                                    borderBottomRightRadius: 4,
                                    paddingBottom: '6%',
                                }}
                                onPress={this.showClippings.bind(this)}>
                                <Image source={imgDownload} resizeMode={'contain'} style={{}}/>
                            </TouchableOpacity>
                        </View>
                    }
                    {
                        //card view of clippings in inbox
                        this.state.showClippings
                        &&
                        <FlatList
                            data={this.state.inboxItems}
                            renderItem={({item}) => this.renderClip(item)}
                            horizontal={false}
                            numColumns={2}
                            columnWrapperStyle={{justifyContent: 'space-evenly'}}
                            keyExtractor={(item, index) => {
                                return String(index);
                            }}
                        />
                    }
                </View>
                <NavMenu>
                    <TouchableOpacity onPress={() => {}}>
                        <Image source={iconCoins} style={{resizeMode: 'contain'}}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {}}>
                        <Image source={iconNetwork} style={{resizeMode: 'contain'}}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        backgroundColor: '#F8F2FF',
                        height: '100%',
                        width: Arivanza.absHeight(.072),
                        borderRadius: Arivanza.absHeight(.072),
                        alignItems: 'center',
                        justifyContent: 'center'
                    }} onPress={() => navigate(Constants.ROUTES.EDITION)}>
                        <Image source={iconEdition} style={[{resizeMode: 'contain'}, {}]}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                    }}>
                        <Image source={iconChat} style={{resizeMode: 'contain'}}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigate(Constants.ROUTES.INBOX)}>
                        <Image source={iconInbox} style={{resizeMode: 'contain'}}/>
                    </TouchableOpacity>
                </NavMenu>
            </SafeAreaView>
        );
    }

    renderClip(item) {
        return (
            <View style={{width: Arivanza.absWidth(.44), height: Arivanza.absHeight(.4), backgroundColor: '#FFFFFF', borderRadius: 4, marginTop: '4%'}}>
                {this.getClipContent(item)}
            </View>
        );
    }

    getClipContent = (item) => {
        switch (item.fileType) {
            case Constants.SHARE_FILE_TYPE.IMAGE:
                return (
                    <View style={{flex: 1, justifyContent: 'flex-start'}}>
                        <Image source={{uri: item.data}} style={{resizeMode: 'contain', flex: 1}}/>
                    </View>
                );
            case Constants.SHARE_FILE_TYPE.VIDEO:
                return (
                    <View style={{flex: 1, justifyContent: 'flex-start'}}>
                        <Video source={{uri: item.data}}   // Can be a URL or a local file.
                               ref={(ref) => {
                                   this.player = ref
                               }}                                      // Store reference
                            // onBuffer={this.onBuffer}                // Callback when remote video is buffering
                            // onEnd={this.onEnd}                      // Callback when playback finishes
                            // onError={this.videoError}               // Callback when video cannot be loaded
                               paused={true}
                               style={{
                                   position: 'absolute',
                                   top: 0,
                                   left: 0,
                                   bottom: 0,
                                   right: 0,
                               }}
                        />
                    </View>
                );
            case Constants.SHARE_FILE_TYPE.YOUTUBE:
                return (
                    <View style={{flex: 1, justifyContent: 'flex-start'}}>
                        <WebView
                            source={{uri: "https://www.youtube.com/embed/"+item.data+"?version=3&enablejsapi=1&rel=0&autoplay=1&showinfo=0&controls=1&modestbranding=0"}}
                            style={{height: '100%', width:'100%', justifyContent:'center', alignItems:'center', backgroundColor:'black'}}
                        />
                    </View>
                );
            case Constants.SHARE_FILE_TYPE.VIMEO:
                return (
                    <View style={{flex: 1, justifyContent: 'flex-start'}}>
                        <WebView
                            source={{uri: "https://player.vimeo.com/video/"+item.data}}
                            style={{height: '100%', width:'100%', justifyContent:'center', alignItems:'center', backgroundColor:'black'}}
                        />
                    </View>
                );
            case Constants.SHARE_FILE_TYPE.AUDIO:
                // return null;
            case Constants.SHARE_FILE_TYPE.URL:
                const url = item.data;
                console.log(url);
                return <WebView source={{uri: url}}/>;
            default:
                return (
                    <Text style={{fontSize: Arivanza.scaleFontSize(17), fontFamily: 'Montserrat-Regular'}}>{item.data}</Text>
                );
        }
    }
}
