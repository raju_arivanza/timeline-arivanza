/**
 * Arivanza React Native App
 * @flow
 */

import React from 'react';
import Constants from './config/Constants';
import {Button, Image, Keyboard, KeyboardAvoidingView, SafeAreaView, ScrollView, StyleSheet, TextInput, TouchableOpacity, View,} from 'react-native';
import Arivanza from "./common/Arivanza";
import {AppHeader} from "./common/AppHeader";
import iconNetwork from '../assets/img/network.png';
import iconChat from '../assets/img/chat-bubble.png';
import iconCoins from '../assets/img/coins.png';
import iconInbox from '../assets/img/letterbox.png';
import iconEdition from '../assets/img/edit.png';
import NavigationBar from 'react-native-navbar-color';
import RichEditor from "./lib/rich-editor";
import {NavMenu} from "./common/NavMenu";

import iconUndo from '../assets/img/iconUndo.png';
import iconText from '../assets/img/iconTextComponents.png';
import iconObjects from '../assets/img/iconObjectComponents.png';
import iconColors from '../assets/img/iconColourComponents.png';
import iconCollapseBar from '../assets/img/iconCollapseBar.png';

export default class Edition extends Arivanza {
    constructor(props, context) {
        super(props, context);
        this.state = {
            text: '',
            keyboardShow:false,
        };
    }

    static navigationOptions = ({navigation}) => {
        return {
            header: (props) => <AppHeader {...props} title={'Edition'}/>,
        };
    };

    async componentDidMount() {
        //set nav bar color
        NavigationBar.setColor('#C18FFF');
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',(event)=>this.keyboardDidShow(event) );
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',(event)=>this.keyboardDidHide(event) );
    };
    keyboardDidShow = (event) => {
        this.setState({keyboardShow:true});
    };

    keyboardDidHide = (event) => {
        this.setState({keyboardShow:false});
    };

    componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    };

    render() {
        const {navigate} = this.props.navigation;
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: '#C18FFF'}}>
                <ScrollView contentContainerStyle={{alignItems: 'center', justifyContent: 'center', marginTop: '5%'}}>
                    <View style={{ width: '80%', height: Arivanza.absHeight(.6), borderRadius: 4, borderColor: '#F2F2F2'}}>
                        <RichEditor setHtml={html => {console.log("HTML ...... ", html)}} showToolBar={false}/>
                    </View>
                </ScrollView>
                {
                    !this.state.keyboardShow
                    &&
                    <NavMenu>
                        <TouchableOpacity onPress={() => {}}>
                            <Image source={iconCoins} style={{resizeMode: 'contain'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {}}>
                            <Image source={iconNetwork} style={{resizeMode: 'contain'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={{
                            backgroundColor: '#F8F2FF',
                            height: '100%',
                            width: Arivanza.absHeight(.072),
                            borderRadius: Arivanza.absHeight(.072),
                            alignItems: 'center',
                            justifyContent: 'center'
                        }} onPress={() => navigate(Constants.ROUTES.EDITION)}>
                            <Image source={iconEdition} style={[{resizeMode: 'contain'}, {}]}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {}}>
                            <Image source={iconChat} style={{resizeMode: 'contain'}}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate(Constants.ROUTES.INBOX)}>
                            <Image source={iconInbox} style={{resizeMode: 'contain'}}/>
                        </TouchableOpacity>
                    </NavMenu>
                }
                {
                    this.state.keyboardShow
                    &&
                    <KeyboardAvoidingView>
                        <View style={{height: 44, backgroundColor: '#F2F2F2', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}}>
                            <TouchableOpacity onPress={() => {}}>
                                <Image source={iconUndo} style={{resizeMode: 'contain'}}/>
                            </TouchableOpacity>
                            <View style={{width: '33%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                                <TouchableOpacity onPress={() => {}}>
                                    <Image source={iconText} style={{resizeMode: 'contain'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => {}}>
                                    <Image source={iconObjects} style={{resizeMode: 'contain'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => {}}>
                                    <Image source={iconColors} style={{resizeMode: 'contain'}}/>
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity onPress={() => {}}>
                                <Image source={iconCollapseBar} style={{resizeMode: 'contain'}}/>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                }
            </SafeAreaView>
        );
    }
}