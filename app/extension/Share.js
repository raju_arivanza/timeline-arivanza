import React, { Component } from 'react';
import isUrl from 'is-url';
import Constants from '../config/Constants';
import _ from 'lodash';
import getVideoId from 'get-video-id';

//images
import logo from '../../assets/img/logo-small.png';
import webPage from '../../assets/img/webpage.png';
import mic from '../../assets/img/mic.png';
import images from '../../assets/img/image-stack.png';
import video from '../../assets/img/video.png';
import downArrow from '../../assets/img/arrow-down.png';
import successTick from '../../assets/img/success_tick.png';
import failureExclaim from '../../assets/img/failure_exclaim.png';
import SharedGroupPreferences from 'react-native-shared-group-preferences';


import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Image,
    Alert, PermissionsAndroid, Platform, Modal
} from 'react-native';
import ShareExtension from "./ShareExtension";
import ComponentBase from "../common/Arivanza";

export default class Share extends ComponentBase {
    constructor(props, context) {
        super(props, context);
        this.state = {
            fileType: '',
            shareContent: '',
            inboxStatus: null,
            isModalVisible: false
        }
    }

    async componentDidMount() {
        try {
            const { type, value } = await ShareExtension.data();
            const {assessedFileType, shareContent} = this._getFileType(type, value);
            this.setState({
                fileType: assessedFileType,
                shareContent: shareContent,
                isModalVisible: true
            });
        } catch(e) {
            console.log('errrr', e)
        }
    }

    _getFileType = (type, value) => {
        console.log("Type", type);
        console.log("Value", value);
        if(type === 'text/plain') {
            if(isUrl(value)) {
                //check if this is youtube or vimeo url
                const {id, service} = getVideoId(value);
                if(id) {
                    if(service === 'youtube') {
                        return {assessedFileType: Constants.SHARE_FILE_TYPE.YOUTUBE, shareContent: id};
                    } else if(service === 'vimeo') {
                        return {assessedFileType: Constants.SHARE_FILE_TYPE.VIMEO, shareContent: id};
                    }
                }
                return {assessedFileType: Constants.SHARE_FILE_TYPE.URL, shareContent: value};
            } else {
                return {assessedFileType: Constants.SHARE_FILE_TYPE.TEXT, shareContent: value};
            }
        } else if(type.indexOf('audio') !== -1) {
            return {assessedFileType: Constants.SHARE_FILE_TYPE.AUDIO, shareContent: value};
        } else if(type.indexOf('video') !== -1) {
            return {assessedFileType: Constants.SHARE_FILE_TYPE.VIDEO, shareContent: value};
        } else if(type.indexOf('images') !== -1) {
            return {assessedFileType: Constants.SHARE_FILE_TYPE.IMAGES, shareContent: value};
        } else if(type.indexOf('image') !== -1) {
            return {assessedFileType: Constants.SHARE_FILE_TYPE.IMAGE, shareContent: value};
        }
        //TODO handle other types
    };

    closeExtension() {
        ShareExtension.close();
        console.log("I closed this one!!");
    }

    async dealWithPermissions(categoryData) {
        try {
            const grantedStatus = await PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
            ]);
            const writeGranted = grantedStatus["android.permission.WRITE_EXTERNAL_STORAGE"] === PermissionsAndroid.RESULTS.GRANTED
            const readGranted = grantedStatus["android.permission.READ_EXTERNAL_STORAGE"] === PermissionsAndroid.RESULTS.GRANTED
            if (writeGranted && readGranted) {
                this.saveInboxDataToSharedStorage(categoryData).then(()=> console.log("Saved saveInboxDataToSharedStorage"))
            } else {
                // You can either limit the user in access to the app's content,
                // or do a workaround where the user's data is saved using only
                // within the user's local app storage using something like AsyncStorage
                // instead. This is only an android issue since it uses read/write external storage.
            }
        } catch (err) {
            console.warn(err)
        }
    }
    async saveInboxDataToSharedStorage(data) {
        try {
            await SharedGroupPreferences.setItem(Constants.KEY_DB_INBOX, data, Constants.APP_GROUP_IDENTIFIER)
        } catch(errorCode) {
            // errorCode 0 = There is no suite with that name
            console.log(errorCode)
        }
    }
    sendToInbox = async () => {
        var stage = Constants.APP_GROUP_IDENTIFIER;
        try {
            // Alert.alert("here??");
            if(!this.state.shareContent) {
                this.closeExtension();
            }

            var inboxStore, sharedData = [{fileType: this.state.fileType, data: this.state.shareContent}];
            if(this.state.fileType === Constants.SHARE_FILE_TYPE.IMAGES) {
                //split the images into multiple items
                sharedData = this.state.shareContent.split(",").map(img => {return {fileType: Constants.SHARE_FILE_TYPE.IMAGE, data: _.trim(img)}});
            }
            try {
                inboxStore = await SharedGroupPreferences.getItem(Constants.KEY_DB_INBOX, Constants.APP_GROUP_IDENTIFIER);
                if(!inboxStore) {
                    inboxStore = [];
                }
                //push the content
                _.forEach(sharedData, data => {
                    inboxStore.push(data);
                });
            } catch(error) {
                inboxStore = sharedData;
            }
            stage = "3";

            console.log("this.state.shareContent", this.state.shareContent);
            // await AsyncStorage.setItem(Constants.KEY_DB_INBOX, JSON.stringify(inboxStore));
            if (Platform.OS === 'android') {
                this.dealWithPermissions(inboxStore);
            } else {
                this.saveInboxDataToSharedStorage(inboxStore).then(()=> console.log("Saved saveInboxDataToSharedStorage"))
            }
            this.setState({inboxStatus: Constants.RESULT.SUCCESS})
        } catch (error) {
            // Error saving data
            console.log("error saving data", error);
            this.errors = error;
            this.setState({inboxStatus: Constants.RESULT.FAILURE})
        }
        setTimeout(() => this.closeExtension(), 1000);
    };
    getContentBasedIcon = () => {
        switch (this.state.fileType) {
            case Constants.SHARE_FILE_TYPE.IMAGES:
                return images;
            case Constants.SHARE_FILE_TYPE.AUDIO:
                return mic;
            case Constants.SHARE_FILE_TYPE.YOUTUBE:
            case Constants.SHARE_FILE_TYPE.VIMEO:
            case Constants.SHARE_FILE_TYPE.VIDEO:
                return video;
            case Constants.SHARE_FILE_TYPE.URL:
                return webPage;
            default:
                return null;
        }
    };
    getContentBasedHelperText = () => {
        switch (this.state.fileType) {
            case Constants.SHARE_FILE_TYPE.IMAGES:
                return 'Create new stack with the selected images';
            case Constants.SHARE_FILE_TYPE.AUDIO:
                return 'Create new stack with the selected audio';
            case Constants.SHARE_FILE_TYPE.YOUTUBE:
            case Constants.SHARE_FILE_TYPE.VIMEO:
            case Constants.SHARE_FILE_TYPE.VIDEO:
                return 'Create new stack with the selected video';
            case Constants.SHARE_FILE_TYPE.URL:
                return 'Create new stack with the contents of the web page';
            default:
                return null;
        }
    };

    getContentBasedContext = () => {
        if(this.state.inboxStatus) {
            //this is post send to inbox case
            return (
                <View style={{flex: 1}}>
                    <View style={{
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                        flex: 1.5
                    }}>
                        <Image source={this.state.inboxStatus === Constants.RESULT.SUCCESS ? successTick : failureExclaim}
                               style={{resizeMode: "contain", width: Share.absWidth(0.32), height: Share.absWidth(0.32)}}/>
                    </View>
                    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{
                            fontFamily: 'Quicksand-Medium',
                            fontSize: Share.scaleFontSize(this.state.inboxStatus === Constants.RESULT.SUCCESS ? 24 : 19),
                            color: '#8C8C8C',
                            textAlign: 'center'
                        }}>
                            {
                                this.state.inboxStatus === Constants.RESULT.SUCCESS
                                    ?
                                    "Saved!"
                                    :
                                    "An error occurred, please try again."
                            }
                        </Text>
                    </View>
                </View>
            );
        } else {
            //loading stage
            if(this.state.fileType === Constants.SHARE_FILE_TYPE.TEXT) {
                return (
                    <Text style={{fontFamily: 'Nunito-Regular', fontSize: Share.scaleFontSize(17)}}>{this.state.shareContent}</Text>
                );
            } else if(this.state.fileType === Constants.SHARE_FILE_TYPE.IMAGE) {
                return (
                    <View style={{flex: 1, justifyContent: 'flex-start'}}>
                        <Image source={{uri: this.state.shareContent}} style={{resizeMode: 'contain', height: '100%', width: '100%'}}/>
                    </View>
                );
            } else {
                return (
                    <View style={{flex: 1}}>
                        <View style={{flex: 1.7, alignItems: 'center', justifyContent: 'flex-end'}}>
                            <Image source={this.getContentBasedIcon()} style={{resizeMode: 'contain'}}/>
                        </View>
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-end'}}>
                            <Image source={downArrow} style={{resizeMode: 'contain'}}/>
                        </View>
                        <View style={{flex: 1.7, alignItems: 'center', justifyContent: 'center', paddingLeft: '8%', paddingRight: '8%'}}>
                            <Text style={{
                                fontFamily: 'Quicksand-Medium',
                                fontSize: Share.scaleFontSize(19),
                                color: '#8C8C8C',
                                textAlign: 'center'
                            }}>
                                {this.getContentBasedHelperText()}
                            </Text>
                        </View>
                    </View>
                );
            }
        }
    };
    render() {
        return (
            <View style={[Platform.OS === 'android' ? {} : {flex: 1, backgroundColor: 'rgba(64, 64, 64, .5)'}]}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.isModalVisible}
                    onRequestClose={() => {}}>
                    <View style={{position: 'absolute', bottom: Share.absHeight(0.1), alignSelf: 'center'}}>
                        <View style={{height: Share.absHeight(.7), alignItems: 'center'}}>
                            <View style={{width: Share.absWidth(.7), flex: 7.65, borderRadius: 4}}>
                                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', borderTopLeftRadius: 4, borderTopRightRadius: 4, backgroundColor: '#3BA39E'}}>
                                    <Image source={logo} style={{resizeMode: 'contain'}}/>
                                </View>
                                <View style={{flex: 6, padding: Share.absWidth(0.027), borderBottomRightRadius: 4, borderBottomLeftRadius: 4, backgroundColor: '#FFFFFF'}}>
                                    {this.getContentBasedContext()}
                                </View>
                            </View>
                            <View style={{width: Share.absWidth(.75), flex: 1}}>
                                <View style={{flex: 1, justifyContent: 'flex-end'}}>
                                    <View style={{height: '81%', flexDirection: 'row'}}>
                                        <TouchableOpacity style={{
                                            flex: 1,
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            backgroundColor: '#F8F2FF',
                                            borderTopLeftRadius: Share.absWidth(.5),
                                            borderBottomLeftRadius: Share.absWidth(.5),
                                        }} onPress={this.closeExtension.bind(this)}>
                                            <Text style={{color: '#FF268B',
                                                fontSize: Share.scaleFontSize(16),
                                                lineHeight: (Share.scaleFontSize(16) * 1.2),//using this since this font has uneven line-height to center itself
                                                fontFamily: 'Quicksand-Medium'}}>Cancel</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{
                                            flex: 1,
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            backgroundColor: '#EBFFFE',
                                            borderTopRightRadius: Share.absWidth(.5),
                                            borderBottomRightRadius: Share.absWidth(.5),
                                        }} onPress={this.sendToInbox.bind(this)}>
                                            <Text style={{color: '#328A85',
                                                fontSize: Share.scaleFontSize(16),
                                                lineHeight: (Share.scaleFontSize(16) * 1.2),//using this since this font has uneven line-height to center itself
                                                fontFamily: 'Quicksand-Medium'}}>Send to Inbox</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}