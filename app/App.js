/**
 * Arivanza React Native App
 * @flow
 */

import React, { Component } from 'react';

import {
    // Platform,
    StyleSheet,
    Text,
    View,
    Image, Dimensions
} from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Chat from "./Chat";
import Network from "./Network";
import Inbox from "./Inbox";

//tab icons
import iconNetwork from '../assets/img/network.png';
import iconChat from '../assets/img/chat-bubble.png';
import iconCoins from '../assets/img/coins.png';
import iconInbox from '../assets/img/letterbox.png';
import iconEdition from '../assets/img/edit.png';
import Constants from "./config/Constants";
import Edition from "./Edition";
import Arivanza from "./common/Arivanza";
import StackViewStyleInterpolator from "react-navigation-stack/dist/views/StackView/StackViewStyleInterpolator";
import Home from "./Home";
import NavigationBar from "react-native-navbar-color";

type Props = {};
export default class App extends Arivanza<Props> {
    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {
        //setting the top bar styling
        NavigationBar.setStatusBarColor('#FFFFFF');
        NavigationBar.setStatusBarTheme('dark');
    }
    render() {
        return <RootStack/>;
    }
}

const RootStack = createStackNavigator(
    {
        Home: Home,
        Network: Network,
        Edition: Edition,
        Chat: Chat,
        Inbox: Inbox,
    },
    {
        initialRouteName: Constants.ROUTES.HOME,
        navigationOptions: {
            header: null, //(props) => <AppHeader {...props} />,
            headerStyle: {
                backgroundColor: '#FFFFFF',
            },
        },
        transitionConfig: () => ({
            screenInterpolator: sceneProps => {
                return StackViewStyleInterpolator.forHorizontal(sceneProps);
            }
        }),
    }
);

// export default createBottomTabNavigator(
//     {
//         Home: App,
//         Network: Network,
//         Edition: Edition,
//         Chat: Chat,
//         Inbox: Inbox,
//     },
//     {
//         initialRouteName: Constants.ROUTES.INBOX,
//         navigationOptions: ({ navigation }) => ({
//             tabBarIcon: ({ focused, tintColor }) => {
//                 const { routeName } = navigation.state;
//                 switch (routeName) {
//                     case Constants.ROUTES.NETWORK:
//                         return <Image source={iconNetwork} color={tintColor} />;
//                     case Constants.ROUTES.CHAT:
//                         return <Image source={iconChat} color={tintColor} />;
//                     case Constants.ROUTES.INBOX:
//                         return <Image source={iconInbox} color={tintColor} />;
//                     case Constants.ROUTES.EDITION:
//                         return (
//                             <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'yellow', borderRadius: App.absWidth()}}>
//                                 <Image source={iconEdition} color={tintColor} style={{resizeMode: 'contain'}}/>
//                             </View>
//                         );
//                     default:
//                         return null;
//                 }
//             },
//         }),
//         tabBarOptions: {
//             activeTintColor: 'tomato',
//             inactiveTintColor: 'gray',
//             showLabel: false,
//             style: {
//                 backgroundColor: 'rgb(235, 255, 254)',
//                 borderRadius: 22,
//                 width: '90%',
//                 alignSelf: 'center',
//             },
//         },
//     }
// );

