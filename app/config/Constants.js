import {Platform} from 'react-native';

export default Constants = {
    KEY_DB_INBOX: 'AZA_INBOX',
    SHARE_FILE_TYPE: {
        IMAGE: 'Image',
        IMAGES: 'Images',
        AUDIO: 'Audio',
        VIDEO: 'Video',
        TEXT: 'Text',
        URL: 'Url',
        YOUTUBE: 'Youtube',
        VIMEO: 'Vimeo',
    },
    RESULT: {
        SUCCESS: 'Success',
        FAILURE: 'Failure',
    },
    APP_GROUP_IDENTIFIER: Platform.select({android: 'com.timeline.arivanza', ios: 'group.com.timeline.arivanza'}),
    DESIGN: {
        WIDTH: 375,
        HEIGHT: 667
    },
    ROUTES: {
        CHAT: "Chat",
        INBOX: "Inbox",
        NETWORK: "Network",
        SEARCH: "Search",
        EDITION: "Edition",
        HOME: "Home",
    }
}