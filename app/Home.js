/**
 * Arivanza React Native App
 * @flow
 */

import React from 'react';
import Constants from './config/Constants';
import {Image, SafeAreaView, TouchableOpacity, View} from 'react-native';
import Arivanza from "./common/Arivanza";
import {AppHeader} from "./common/AppHeader";
import iconNetwork from '../assets/img/network.png';
import iconChat from '../assets/img/chat-bubble.png';
import iconCoins from '../assets/img/coins.png';
import iconInbox from '../assets/img/letterbox.png';
import iconEdition from '../assets/img/edit.png';
import NavigationBar from 'react-native-navbar-color'
import {NavMenu} from "./common/NavMenu";

export default class Home extends Arivanza {
    constructor(props, context) {
        super(props, context);
    }

    static navigationOptions = ({navigation}) => {
        return {
            header: (props) => <AppHeader {...props} title={'Home'} titleTextColor={'#28706D'}/>,
        };
    };

    componentDidMount() {
        //set nav bar color
        NavigationBar.setColor('#4ED7D0');
    }
    render() {
        const {navigate} = this.props.navigation;
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: '#4ED7D0'}}>
                <View style={{flex: 1}}>

                </View>
                <NavMenu>
                    <TouchableOpacity onPress={() => {
                    }}>
                        <Image source={iconCoins} style={{resizeMode: 'contain'}}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                    }}>
                        <Image source={iconNetwork} style={{resizeMode: 'contain'}}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        backgroundColor: '#F8F2FF',
                        height: '100%',
                        width: Arivanza.absHeight(.072),
                        borderRadius: Arivanza.absHeight(.072),
                        alignItems: 'center',
                        justifyContent: 'center'
                    }} onPress={() => navigate(Constants.ROUTES.EDITION)}>
                        <Image source={iconEdition} style={[{resizeMode: 'contain'}, {}]}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                    }}>
                        <Image source={iconChat} style={{resizeMode: 'contain'}}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigate(Constants.ROUTES.INBOX)}>
                        <Image source={iconInbox} style={{resizeMode: 'contain'}}/>
                    </TouchableOpacity>
                </NavMenu>
            </SafeAreaView>
        );
    }
}
