import { AppRegistry } from 'react-native';
import App from './app/App';
import Share from './app/extension/Share'

AppRegistry.registerComponent('Arivanza', () => App);
AppRegistry.registerComponent('ArivanzaShareExtension', () => Share);
